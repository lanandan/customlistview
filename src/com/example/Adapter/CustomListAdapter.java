package com.example.Adapter;
import com.example.customlistview.*;
import java.util.ArrayList;


import android.R.color;
import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

public class CustomListAdapter extends BaseAdapter{
ArrayList<String> item;
Context context;
public LayoutInflater inflator;
	public CustomListAdapter(ArrayList<String> item, Context context) {
	super();
	
	this.item = item;
	this.context = context;
}

	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return item.size();
	}

	@Override
	public Object getItem(int position) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public long getItemId(int position) {
		// TODO Auto-generated method stub
		return 0;
	}
class ViewHolder
{TextView tv1;
	}
	@SuppressLint("InflateParams")
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		// TODO Auto-generated method stub
		ViewHolder holder=new ViewHolder();
		if(convertView==null)
		{	inflator=(LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView=inflator.inflate(R.layout.listitem_single,null);
			holder.tv1=(TextView)convertView.findViewById(R.id.txt_singleitem);
			convertView.setTag(holder);
						
		}
		else
		{
			holder=(ViewHolder)convertView.getTag();
			
		}
		holder.tv1.setText(item.get(position));

		return convertView;
	}

}
