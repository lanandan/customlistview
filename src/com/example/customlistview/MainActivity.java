package com.example.customlistview;

import java.util.ArrayList;


import com.example.Adapter.CustomListAdapter;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class MainActivity extends Activity {
ArrayList<String> item;
String[] items={"idly","poori","pongal","softdrink"};
int i;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		ListView li=(ListView)findViewById(R.id.lst_items);
		item=new ArrayList<String>();
		for(i=0;i<items.length;i++){
			item.add(items[i]);
		}
	
		
		CustomListAdapter adapter=new CustomListAdapter(item, getApplicationContext());
		li.setAdapter(adapter);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
